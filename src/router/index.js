import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "admin",
    component: () => import("../views/Layout/index.vue"),
    children: [
      {
        path: "/",
        name: "homepage",
        component: () => import("../components/HomePage/homepage.vue"),
      },
      {
        path: "gua",
        name: "gua",
        meta: {
          // meta用于配置自定义数据，login_required为true表示需要登录
          login_required: true,
        },  
        component: () => import("../components/Registration/register.vue"),
      },
      {
        path: "chu",//出诊信息
        name: "chu",
        component: () => import("../components/Outcalls/chu.vue"),
      },
      {
        path: "notices",
        name: "notices",
        component: () => import("../components/Notice/notices.vue"),
      },
      {
        path: "notice",
        name: "notice",
        component: () => import("../components/Notice/notice.vue"),
      },
      {
        path: "personage",
        name: "personage",
        meta: {
          // meta用于配置自定义数据，login_required为true表示需要登录
          login_required: true,
        },    
        component: () => import("../components/Personage/personage.vue"),
      },
      {
        path: "houzhen",
        name: "houzhen",
        meta: {
          // meta用于配置自定义数据，login_required为true表示需要登录
          login_required: true,
        },    
        component: () => import("../components/houzhen/houzhen.vue"),
      },
      {
        path: "article",
        name: "article",
        component: () => import("../components/Article/article.vue")
      },
      {
        path: "login",
        name: "login",    
        component: () => import("../components/Personage/login.vue"),
      },
      {
        path: "register",
        name: "register", 
          
        component: () => import("../components/Personage/register.vue"),
      },
      {
        path: "/detail/:id",
        name: "detail",
        component: () => import("../components/Notice/NewsDetail.vue"),
      },
      {
        path: "/introduce/:id",
        name: "introduce",
        component: () => import("../components/Outcalls/introduce.vue"),
      },
      {
        path: "/arret1",
        // 新页面
        name: "arret1",
        component: () => import("../components/Notice/arret1.vue"),
      },
      {
        path: "/special",
        name: "special",
        meta: {
          // meta用于配置自定义数据，login_required为true表示需要登录
          login_required: true,
        },  
        component: () => import("../components/Registration/special.vue"),
      },
      {
        path: "NAT",
        name: "NAT",
        meta: {
          // meta用于配置自定义数据，login_required为true表示需要登录
          login_required: true,
        },  
        component: () => import("../components/NAT/NAT.vue"),
      }
    ],
  },
  
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
// 添加全局前置导航守卫
router.beforeEach((to, from, next) => {
  let logged_in = false; // 先将登录状态设置为未登录。
  if (sessionStorage.login) {
    // 检查session中是否已经存储登录状态
    // 如果已经存储，则登录状态为真
    logged_in = true;
  }

  if (
    !logged_in &&
    to.matched.some((item) => {
      return item.meta.login_required;
    })
  ) {
    // 如果未登录，且路由目标配置过需要登录
    // 跳转到登录页面
    next("/login");
  } else {
    // 否则跳转到路由目标
    next();
  }
});


export default router;   